<?php
return [
    '' => 'site/index',
    'bot/webhook/<id:\d+>/<webhook:\d+>' => 'bot/webhook',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
];