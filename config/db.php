<?php

return [
    'class' => yii\db\Connection::class,
    'dsn' => $_ENV['MYSQL_DSN'],
    'username' => $_ENV['MYSQL_USERNAME'],
    'password' => $_ENV['MYSQL_PASSWORD'],
    'charset' => 'utf8',
];