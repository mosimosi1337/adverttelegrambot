<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$urlRules = require __DIR__ . '/_urlRules.php';

$config = [
    'id' => 'Bot',
    'name' => 'AdvertTelegramBot',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'authManager' => [
            'class' => yii\rbac\DbManager::class,
        ],

        'redis' => [
            'class' => yii\redis\Connection::class,
            'hostname' => $_ENV['REDIS_HOST_NAME'],
            'port' => $_ENV['REDIS_PORT'],
            'database' => $_ENV['REDIS_DATABASE'],
            'password' => $_ENV['REDIS_PASSWORD'],
        ],

        'request' => [
            'baseUrl' => '',
            'cookieValidationKey' => $_ENV['COOKIE_KEY'],
            'parsers' => [
                'application/json' => yii\web\JsonParser::class,
            ],
        ],

        'cache' => [
            'class' => yii\caching\FileCache::class,
        ],

        'telegram' => [
            'class' => app\components\TelegramComponent::class,
        ],

        'user' => [
            'identityClass' => app\models\User::class,
            'enableAutoLogin' => true,
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'mailer' => [
            'class' => yii\swiftmailer\Mailer::class,
            'useFileTransport' => true,
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => yii\log\FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'db' => $db,

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => $urlRules,
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
