# **Установка проекта:**

1) ***Установка композера (_Собирает все неообходимые пакеты_):*** В терминале, в папке проекта прописываем `composer install`.


2) ***Создание БД:*** phpMyAdmins в OpenServer необходимо создать бд с расширениеем `utf8_general_ci`, логин и пароль дефолтно
   стоит `username = root; password = пустая строка`.


3) ***Настройка переменных окружения:*** пример файла `.env.dist`, перенести эти переменные и  подставить свои значения в `.env`, в корневой директории проекта.


4) ***Запуск миграции:*** если подключения к базе данных в `.env` файле указано верно, прописываем в терминале `yii migrate --migrationPath=@yii/rbac/migrations`, следующая команда `yii migrate`.


5) ***Добавление пользователя для входа:*** в терминале прописываем `mysql -h 127.0.0.1 -u <ЛОГИН ОТ БД> -p`, затем нужно ввести пароль от БД если имеется. Даллее идет команда(_`Заменять значение <>`_) `INSERT user(username, password, accessToken, email)
   VALUES ('<Твой личный юзернейм>', '<пароль>', '<рандомная строка>', '<рандомная строка>', '<email>');`.


6) ***Вход в приложение:***  в `/controllers/SiteControllers` в методе `actionLogin()`  заменяешь 
    
        if ($params !== [] && $model->load($params) && $model->validate()) {
            $user = User::findOne(['username' => $model->username]);

            if ($user !== null && sodium_crypto_pwhash_str_verify($user->password, $model->password)) {
                $model->password = password_hash($model->password, PASSWORD_ARGON2I);
                $model->login();

                $this->redirect('index');
            }

            $model->addError('username', 'Не правильное имя пользователя или пароль');
        }

    на

        if ($params !== [] && $model->load($params) && $model->validate()) {
            $model->login();

            $auth = \Yii::$app->authManager;
            $userRole = $auth->getRole(User::ROLE_ADMIN);
            $auth->assign($userRole, \Yii::$app->user->identity->id);

            $this->redirect(['index']);
        }

    после того как зашел, создаешь нового пользователя с ролью **admin**, возвращаешь код который менял, обратно, **перезаходишь через нового пользователя**, и **удаляешь старого**.