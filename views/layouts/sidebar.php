<?php

use hoaaah\sbadmin2\widgets\Menu;

?>

<?=Menu::widget([
    'options' => [
        'ulClass' => "navbar-nav sidebar bg-gradient-success rounded-right sidebar-dark accordion",
        'ulId' => "accordionSidebar",
    ],
    'brand' => [
        'url' => ['/'],
        'content' => <<<HTML
            <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-robot"></i>
            </div>
            <div class="sidebar-brand-text mx-3">www.atb.ru</div>
        HTML,
    ],
    'items' => [
        [
            'label' => 'Dashboard',
            'url' => ['/site/index'],
            'icon' => 'fas fa-fw fa-tachometer-alt',
            'visible' => true,
        ],
        [
            'label' => 'Статистика',
            'icon' => 'fa fa-chart-line',
            'url' => ['#'],
        ],
        [
            'label' => 'Боты',
            'icon' => 'fa fa-robot',
            'items' => [
                [
                    'label' => 'Список',
                    'icon' => 'fa fa-list',
                    'url' => ['/bot/index'],
                ],
                [
                    'label' => 'Добавить нового',
                    'icon' => 'fa fa-plus',
                    'url' => ['/bot/create'],
                    'visible' => \Yii::$app->user->identity->isAdmin(),
                ],
            ],
        ],
        [
            'label' => 'Пользователи',
            'icon' => 'fa fa-user',
            'visible' => true,
            'items' => [
                [
                    'label' => 'Список',
                    'icon' => 'fa fa-list',
                    'url' => ['/user/index'],
                ],
                [
                    'label' => 'Добавить нового',
                    'icon' => 'fa fa-user-plus',
                    'url' => ['/user/create'],
                    'visible' => \Yii::$app->user->identity->isAdmin(),
                ],
            ],
        ],
        [
            'label' => 'Телеграм',
            'icon' => 'fa fa-at',
            'items' => [
                [
                    'label' => 'Аккаунты',
                    'url' => /*['/telegram/account']*/ ['#'],
                    'icon' => 'fa fa-users',
                ],
            ],
        ],
    ],
]);