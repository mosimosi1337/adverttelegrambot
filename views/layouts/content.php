<?php
/**
 * @var $content string
 */

use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Alert;
use yii\helpers\Html;
use yii\helpers\Inflector;

?>
<div class="container-fluid">

    <div class="row">
        <div class="col-md-5 ml-4">
            <div class="d-sm-flex align-items-center justify-content-start mb-4">
                <?php if (!\Yii::$app->user->isGuest): ?>
                    <?php if (isset($this->blocks['content-header'])) : ?>
                        <h1 class="h3 mb-0 text-gray-800"><?=$this->blocks['content-header']?></h1>
                    <?php else : ?>
                        <div class="h3 mb-0 text-gray-800">
                            <?php if ($this->title !== null) : ?>
                                <?=Html::encode($this->title);?>
                            <?php else : ?>
                                <?=Inflector::camel2words(
                                    Inflector::id2camel($this->context->module->id)
                                );?>
                                <?=($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php if (Yii::$app->getSession()->getAllFlashes()) {
                foreach (Yii::$app->getSession()->getAllFlashes() as $key => $value) {
                    echo Alert::widget([
                        'options' => [
                            'class' => 'alert-' . $key,
                        ],
                        'body' => $value,
                    ]);
                }
            } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?=$content?>
        </div>
    </div>
</div>
