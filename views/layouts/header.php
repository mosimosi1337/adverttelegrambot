<?php

use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow rounded-bottom">
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>

    <!-- Topbar Search -->
    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
        <div class="input-group">
            <input type="text" class="form-control bg-light border-0 small" placeholder="Поиск..." aria-label="Search"
                   aria-describedby="basic-addon2">
            <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                    <i class="fas fa-search fa-sm"></i>
                </button>
            </div>
        </div>
    </form>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?=User::findOne(Yii::$app->user->id)->username?></span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/random/60x60">
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?=Url::to(['user/profile/' . Yii::$app->user->id]);?>">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Профиль
                </a>
                <a class="dropdown-item" href="<?=Url::to(['site/setting/' . Yii::$app->user->id]);?>">
                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                    Настройки
                </a>
                <?php if (\Yii::$app->user->identity->isAdmin()): ?>
                    <a class="dropdown-item" href="<?=Url::to(['site/config']);?>">
                        <i class="fas fa-server fa-sm fa-fw mr-2 text-gray-400"></i>
                        Конфигурация сервера
                    </a>
                <?php endif; ?>
                <div class="dropdown-divider"></div>
                <?=Html::a(
                    'Выход',
                    ['site/logout'],
                    ['data-method' => 'get', 'class' => 'dropdown-item']
                )?>
            </div>
        </li>
    </ul>
</nav>
