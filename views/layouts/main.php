<?php
/**
 * @var $content string
 */

use app\assets\AppAsset;
use hoaaah\sbadmin2\assets\SbAdmin2Asset;
use yii\helpers\Html;

AppAsset::register($this);
SbAdmin2Asset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@bower/startbootstrap-sb-admin-2');
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body id="page-top">

<?php $this->beginBody() ?>

<div id="wrapper">


    <?php if (!\Yii::$app->user->isGuest): ?>
        <?= $this->render(
            'sidebar.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>
    <?php endif; ?>

    <div id="content-wrapper" class="d-flex flex-column">

        <div id="content">

            <?php if (!\Yii::$app->user->isGuest): ?>
                <?= $this->render(
                    'header.php',
                    ['directoryAsset' => $directoryAsset]
                ) ?>
            <?php endif; ?>


            <?= $this->render(
                'content.php',
                ['content' => $content, 'directoryAsset' => $directoryAsset]
            ) ?>

        </div>

    </div>

</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
