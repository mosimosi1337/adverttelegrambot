<?php
/**
 * @var $user User
 * @var $this View
 */


use app\models\User;
use kartik\form\ActiveForm;
use yii\web\View;

$this->title = 'Сброс пароля для пользователя ' . \Yii::$app->user->identity->username;
?>

<div class="row justify-content-center">
    <div class="col-md-10">
        <?php
        $form = ActiveForm::begin([
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "<div class='row'>{label}</div>{input}{error}",
                'labelOptions' => ['class' => 'col-lg-3 text-gray-600 control-label'],
            ],
        ])
        ?>
        <?= $form->field($user, 'password')->textInput(['value' => ''])->label('Новый пароль') ?>

        <div class="form-group float-right mt-3">
            <button type="submit" class="btn btn-success px-3">Обновить</button>
        </div>

    </div>
    <?php ActiveForm::end() ?>
</div>
