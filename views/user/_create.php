<?php
/**
 * @var $this yii\web\View
 * @var $model RegisterUserForm
 */

use app\models\User;
use app\models\user\RegisterUserForm;
use kartik\widgets\Select2;
use kartik\form\ActiveForm;

$this->title = 'Создание нового пользователя'
?>

<style>
    @media (max-width: 998px) {
        .mt-resize {
            margin-top: 5vh;
        }
    }
</style>

<div class="row mx-4">
    <div class="col-md-6 p-4 bg-white shadow rounded-lg">
        <?php
        $form = ActiveForm::begin([
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "<div class='row'>{label}</div>{input}{error}",
                'labelOptions' => ['class' => 'col-lg-3 text-gray-600 control-label'],
            ],
        ])
        ?>
        <?=$form->field($model, 'username')
            ->textInput([
                'class' => 'form-control bg-light border-0 small',
                'placeholder' => 'Введите имя пользователя...',
            ])
            ->label('Имя пользователя')?>

        <?=$form->field($model, 'email')
            ->textInput([
                'class' => 'form-control bg-light border-0 small',
                'placeholder' => 'Введите Email...',
            ])?>

        <?=$form->field($model, 'password')
            ->textInput([
                'class' => 'form-control bg-light border-0 small',
                'placeholder' => 'Введите пароль...',
            ])
            ->label('Пароль')?>

        <div class="form-group float-right mt-3">
            <button type="submit" class="btn btn-success px-3">Создать</button>
        </div>
    </div>

    <div class="col-md-6 mt-resize" <?=\Yii::$app->user->identity->isAdmin() ? '' : 'hidden'?>>
        <div class="p-4 bg-white shadow rounded-lg ">
            <?=$form->field($model, 'role')->widget(Select2::class, [
                'model' => RegisterUserForm::class,
                'attribute' => 'role',
                'data' => User::roles(),
                'hideSearch' => true,
                'options' => [
                    'class' => 'form-control bg-light border-0 small',
                    'placeholder' => 'Выберите роль...',
                ],
            ])->label('Роль');
            ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>
</div>

