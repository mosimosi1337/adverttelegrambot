<?php
/**
 * @var $dataProvider ActiveDataProvider
 * @var $this yii\web\View
 */

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\grid\ActionColumn;

$this->title = 'Пользователи';
?>

    <div class="row justify-content-center ">
        <div id="hidden-shadow" class="col-lg-11 bg-white shadow bg-light rounded-lg">
            <?=GridView::widget([
                'dataProvider' => $dataProvider,
                'summaryOptions' => ['class' => 'mt-3 text-gray-700'],
                'tableOptions' => ['class' => 'table my-3 ',],
                'options' => ['class' => 'custom-scroll-table p-3'],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'label' => 'ID',
                    ],
                    'username',
                    'email',
                    'number',
                    'chat_id',
                    [
                        'class' => ActionColumn::class,
                        'template' => "{update} {delete}",
                        'visible' => Yii::$app->user->identity->isAdmin(),
                    ],
                ],
            ]);?>
        </div>
    </div>

<?php
$jsCode = <<<JS

window.onresize = func; // при изменении размеров браузера
window.onload   = func;

function func(){
    if (window.screen.width <= 998) {
        $('#hidden-shadow').removeClass('shadow');
        $('#hidden-shadow').removeClass('bg-white');
    } else {
        $('#hidden-shadow').addClass('shadow');
        $('#hidden-shadow').addClass('bg-white');
    }
}
JS;

$this->registerJs($jsCode, \yii\web\View::POS_LOAD);
