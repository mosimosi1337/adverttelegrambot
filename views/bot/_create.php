<?php
/**
 * @var $this View
 * @var $model RegisterBotForm
 */

use app\models\bot\RegisterBotForm;
use kartik\form\ActiveForm;
use yii\web\View;

$this->title = 'Создание Telegram бота';
?>

<div class="row mx-4">
    <div class="col-md-12 p-4 bg-white shadow rounded-lg">
        <?php $form = ActiveForm::begin([
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'labelOptions' => ['class' => 'col-lg-3 text-gray-600 control-label'],
            ],
        ]) ?>

        <?=$form->field($model, 'title')
            ->textInput([
                'class' => 'form-control bg-light border-0 small',
                'placeholder' => 'Введите имя название бота...',
            ])?>

        <?=$form->field($model, 'path')
            ->textInput([
                'class' => 'form-control bg-light border-0 small',
                'placeholder' => 'Введите путь...',
            ])?>

        <?=$form->field($model, 'token')
            ->textInput([
                'class' => 'form-control bg-light border-0 small',
                'placeholder' => 'Введите токен бота...',
            ])?>

        <?=$form->field($model, 'controller_name')
            ->textInput([
                'class' => 'form-control bg-light border-0 small',
                'placeholder' => 'Введите название контроллера...',
            ])?>

        <?=$form->field($model, 'webhooks_is_set')
            ->checkbox(['template' => '<div class="ml-1">{input}{label}{error}</div>'])?>

        <div class="form-group float-right mt-3 chec">
            <button type="submit" class="btn btn-success px-3">Создать</button>
        </div>
    </div>
    <?php ActiveForm::end() ?>
</div>