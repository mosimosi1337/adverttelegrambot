<?php
/**
 * @var $this View
 * @var $dataProvider ActiveDataProvider
 */

use app\models\bot\Bot;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

$this->title = 'Список ботов';
?>

    <div class="row justify-content-center ">
        <div id="hidden-shadow" class="col-lg-11 bg-white shadow bg-light rounded-lg">
            <?=GridView::widget([
                'dataProvider' => $dataProvider,
                'summaryOptions' => ['class' => 'mt-3 text-gray-700'],
                'tableOptions' => ['class' => 'table my-3 ',],
                'options' => ['class' => 'custom-scroll-table p-3'],
                'columns' => [
                    [
                        'attribute' => 'id',
                        'label' => 'ID',
                    ],
                    'title',
                    [
                        'attribute' => 'path',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::tag(
                                'a',
                                $data->path,
                                ['href' => 'https://t.me/' . str_replace('@', '', $data->path)]);
                        },
                    ],
                    'token',
                    [
                        'attribute' => 'webhooks_is_set',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::tag(
                                'a',
                                Bot::getWebhooksHtml($data->webhooks_is_set),
                                ['href' => Url::to(['bot/webhook/' . $data->id . '/' . $data->webhooks_is_set])]
                            );
                        },
                    ],
                    [
                        'class' => ActionColumn::class,
                        'template' => "{update} {delete}",
                        'visible' => Yii::$app->user->identity->isAdmin(),
                    ],
                ],
            ]);?>
        </div>
    </div>

<?php
$jsCode = <<<JS

window.onresize = func; // при изменении размеров браузера
window.onload   = func;

function func(){
    if (window.screen.width <= 1600) {
        $('#hidden-shadow').removeClass('shadow');
        $('#hidden-shadow').removeClass('bg-white');
    } else {
        $('#hidden-shadow').addClass('shadow');
        $('#hidden-shadow').addClass('bg-white');
    }
}
JS;

$this->registerJs($jsCode, \yii\web\View::POS_LOAD);
