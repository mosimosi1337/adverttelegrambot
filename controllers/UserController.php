<?php


namespace app\controllers;


use app\models\user\RegisterUserForm;
use app\models\User;
use app\models\user\search\UserSearch;
use Exception;
use Throwable;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use \yii\web\Response;

class UserController extends Controller
{
    /**
     * @return array[]
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => [],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [User::ROLE_USER],
                    ],
                    [
                        'actions' => ['create', 'delete', 'update'],
                        'allow' => true,
                        'roles' => [User::ROLE_MODER],
                    ],
                    [
                        'actions' => ['create', 'delete', 'update'],
                        'allow' => false,
                        'roles' => [User::ROLE_USER],
                        'denyCallback' => function ($rule, $action) {
                            return $action->controller->redirect(['user/index']);
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'create' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $params = \Yii::$app->request->get();

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search($params);

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @return string
     * @throws Exception
     */
    public function actionCreate(): string
    {
        $model = new RegisterUserForm();
        $request = \Yii::$app->request;

        if ($request->isPost && $model->load($request->post()) && $model->validate()) {
            $user = new User([
                'email' => $model->email,
                'username' => $model->username,
                'authKey' => hash('sha256', random_bytes(5)),
                'accessToken' => hash('sha256', random_bytes(5)),
                'password' => password_hash($model->password, PASSWORD_ARGON2I),
            ]);

            $user->save();

            $auth = \Yii::$app->authManager;
            $userRole = $auth->getRole($model->role ?: User::ROLE_USER);
            $auth->assign($userRole, $user->getId());

            $this->redirect('/user/index');
        }

        return $this->render('_create', compact('model'));
    }

    /**
     * @param int $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete(int $id): Response
    {
        $user = User::findOne($id);
        $user->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionUpdate(int $id): string
    {
        $params = \Yii::$app->request->post();
        $user = User::findOne($id);

        if ($user->load($params) && $user->validate()) {
            $user->password = password_hash($user->password, PASSWORD_ARGON2I);
            $user->save();
        }

        return $this->render('_reset_password', compact('user'));
    }

}