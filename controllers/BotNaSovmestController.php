<?php

namespace app\controllers;

use yii\httpclient\Exception;

/**
 * @property-read array $commands
 */
class BotNaSovmestController extends AbstractBotController
{
    /**
     * @return string[]
     */
    public function getCommands(): array
    {
        return [
            '/start' => 'commandStart',
        ];
    }

    /**
     * @throws Exception
     */
    public function commandStart(): void
    {
        $this->storeSet($this->chatId, self::STORE_START, null);
        $this->component->sendPhoto(
            $this->chatId,
            'https://as1.ftcdn.net/v2/jpg/04/11/70/86/1000_F_411708654_LZSzqQq4s0A3tusU7Gbl3tD2VG0BCxRO.jpg',
            $this->render('advert'),
            null
        );


        $question = $this->storeGet($this->chatId, 'question');

        if ($question !== null) {
            $this->storeSet($this->chatId, 'question', null);
        }

        $this->component->sendPhoto(
            $this->chatId,
            'https://images.pexels.com/photos/248148/pexels-photo-248148.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=750&w=1260',
            $this->render('question_1'),
            json_encode([
                'inline_keyboard' => [
                    [
                        [
                            "text" => "Одинаково важны",
                            'callback_data' => 'q1_1',
                        ],
                    ],
                    [
                        [
                            "text" => "Любовь, секс на втором плане",
                            'callback_data' => 'q1_2',
                        ],
                    ],
                    [
                        ["text" => "Мы просто спим вместе и о любви не думаем",
                            'callback_data' => 'q1_3',
                        ],
                    ],
                ],
            ], true)
        );
    }

    /**
     * @param $messageText
     * @param $messageId
     * @return void
     * @throws Exception
     */
    public function processMessage($messageText, $messageId)
    {
        $this->delete($this->chatId, (int)$messageId);

        switch (substr($messageText, 0, 3)) {
            case 'q1_':
                $this->component->sendPhoto(
                    $this->chatId,
                    'https://images.pexels.com/photos/1450155/pexels-photo-1450155.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                    $this->render('question_2'),
                    json_encode([
                        'inline_keyboard' => [
                            [
                                [
                                    "text" => "Как можно чаще",
                                    'callback_data' => 'q2_1',
                                ],
                            ],
                            [
                                [
                                    "text" => "Только в постели, это придает сексу остроту",
                                    'callback_data' => 'q2_2',
                                ],
                            ],
                            [
                                [
                                    "text" => "Эти телячьи нежности меня нервируют",
                                    'callback_data' => 'q2_3',
                                ],
                            ],

                        ],
                    ], true)
                );
                $this->storeSet($this->chatId, 'question', $this->storeGet($this->chatId, 'question') . ' ' . $messageText);
                break;
            case 'q2_':
                $this->component->sendPhoto(
                    $this->chatId,
                    'https://images.pexels.com/photos/351119/pexels-photo-351119.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                    $this->render('question_3'),
                    json_encode([
                        'inline_keyboard' => [
                            [
                                [
                                    "text" => "Конечно, но не постоянно",
                                    'callback_data' => 'q3_1',
                                ],
                            ],
                            [
                                [
                                    "text" => "Только тогда, когда об этом просит мужчина",
                                    'callback_data' => 'q3_2',
                                ],
                            ],
                            [
                                [
                                    "text" => "Нет, это мужское дело",
                                    'callback_data' => 'q3_3',
                                ],
                            ],
                        ],
                    ], true)
                );
                $this->storeSet($this->chatId, 'question', $this->storeGet($this->chatId, 'question') . ' ' . $messageText);

                break;
            case 'q3_':
                $this->component->sendPhoto(
                    $this->chatId,
                    'https://images.pexels.com/photos/4980381/pexels-photo-4980381.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                    $this->render('question_4'),
                    json_encode([
                        'inline_keyboard' => [
                            [
                                [
                                    "text" => "Да, в любви должна быть откровенность",
                                    'callback_data' => 'q4_1',
                                ],
                            ],
                            [
                                [
                                    "text" => "Неохотно",
                                    'callback_data' => 'q4_2',
                                ],
                            ],
                            [
                                [
                                    "text" => "Нет, эта тема не для меня",
                                    'callback_data' => 'q4_3',
                                ],
                            ],

                        ],
                    ], true)
                );
                $this->storeSet($this->chatId, 'question', $this->storeGet($this->chatId, 'question') . ' ' . $messageText);

                break;
            case 'q4_':
                $this->component->sendPhoto(
                    $this->chatId,
                    'https://images.pexels.com/photos/5187591/pexels-photo-5187591.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                    $this->render('question_5'),
                    json_encode([
                        'inline_keyboard' => [
                            [
                                [
                                    "text" => "Хороши",
                                    'callback_data' => 'q5_1',
                                ],
                            ],
                            [
                                [
                                    "text" => "Я делаю это, чтобы ни потерять партнера",
                                    'callback_data' => 'q5_2',
                                ],
                            ],
                            [
                                [
                                    "text" => "Я мог (могла) бы везде и при любом случае",
                                    'callback_data' => 'q5_3',
                                ],
                            ],

                        ],
                    ], true)
                );
                $this->storeSet($this->chatId, 'question', $this->storeGet($this->chatId, 'question') . ' ' . $messageText);

                break;
            case 'q5_':
                $this->component->sendPhoto(
                    $this->chatId,
                    'https://images.pexels.com/photos/6202039/pexels-photo-6202039.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                    $this->render('question_6'),
                    json_encode([
                        'inline_keyboard' => [
                            [
                                [
                                    "text" => "Да, но у меня есть определенные недостатки",
                                    'callback_data' => 'q6_1',
                                ],
                            ],
                            [
                                [
                                    "text" => "Нет, и я не знаю, как я могу что-то изменить",
                                    'callback_data' => 'q6_2',
                                ],
                            ],
                            [
                                [
                                    "text" => "Да, у меня все классно",
                                    'callback_data' => 'q6_3',
                                ],
                            ],

                        ],
                    ], true)
                );
                $this->storeSet($this->chatId, 'question', $this->storeGet($this->chatId, 'question') . ' ' . $messageText);

                break;
            case 'q6_':
                $this->component->sendPhoto(
                    $this->chatId,
                    'https://images.pexels.com/photos/6621705/pexels-photo-6621705.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                    $this->render('question_7'),
                    json_encode([
                        'inline_keyboard' => [
                            [

                                [
                                    "text" => "Нормально, ведь это уже позади",
                                    'callback_data' => 'q7_1',
                                ],
                            ],
                            [
                                [
                                    "text" => "Я нахожу это бестактным, но примеряюсь",
                                    'callback_data' => 'q7_2',
                                ],
                            ],
                            [
                                [
                                    "text" => "Резко отрицательно",
                                    'callback_data' => 'q7_3',
                                ],

                            ],
                        ],
                    ], true)
                );
                $this->storeSet($this->chatId, 'question', $this->storeGet($this->chatId, 'question') . ' ' . $messageText);

                break;
            case 'q7_':
                $this->component->sendPhoto(
                    $this->chatId,
                    'https://images.pexels.com/photos/5219080/pexels-photo-5219080.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                    $this->render('question_8'),
                    json_encode([
                        'inline_keyboard' => [
                            [
                                [
                                    "text" => "Без прелюдии секс — не секс",
                                    'callback_data' => 'q8_1',
                                ],
                            ],
                            [
                                [
                                    "text" => "Не много нежностей должно присутствовать, но не долго",
                                    'callback_data' => 'q8_2',
                                ],
                            ],
                            [
                                [
                                    "text" => "У меня нет времени, я хочу секс и сразу же",
                                    'callback_data' => 'q8_3',
                                ],
                            ],
                        ],
                    ], true)
                );
                $this->storeSet($this->chatId, 'question', $this->storeGet($this->chatId, 'question') . ' ' . $messageText);

                break;
            case 'q8_':
                $this->component->sendPhoto(
                    $this->chatId,
                    'https://images.pexels.com/photos/6005657/pexels-photo-6005657.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                    $this->render('question_9'),
                    json_encode([
                        'inline_keyboard' => [
                            [
                                [
                                    "text" => "Как можно дольше",
                                    'callback_data' => 'q9_1',
                                ],
                            ],
                            [
                                [
                                    "text" => "Пока оба не удовлетворяться",
                                    'callback_data' => 'q9_2',
                                ],
                            ],
                            [
                                [
                                    "text" => "До моего оргазма",
                                    'callback_data' => 'q9_3',
                                ],
                            ],
                        ],
                    ], true)
                );
                $this->storeSet($this->chatId, 'question', $this->storeGet($this->chatId, 'question') . ' ' . $messageText);

                break;
            case 'q9_':
                $this->component->sendPhoto(
                    $this->chatId,
                    'https://images.pexels.com/photos/5915370/pexels-photo-5915370.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                    $this->render('question_10'),
                    json_encode([
                        'inline_keyboard' => [
                            [
                                [
                                    "text" => "Да, это показывает мне, что я до экстаза",
                                    'callback_data' => 'q10_1',
                                ],
                            ],
                            [
                                [
                                    "text" => "Мне все равно",
                                    'callback_data' => 'q10_2',
                                ],
                            ],
                            [
                                [
                                    "text" => "Нет, мне бы это мешало сконцентрироваться",
                                    'callback_data' => 'q10_3',
                                ],
                            ],
                        ],
                    ], true)
                );
                $this->storeSet($this->chatId, 'question', $this->storeGet($this->chatId, 'question') . ' ' . $messageText);

                break;
            case 'q10':
                $this->component->sendPhoto(
                    $this->chatId,
                    'https://images.pexels.com/photos/6776115/pexels-photo-6776115.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                    $this->render('question_11'),
                    json_encode([
                        'inline_keyboard' => [
                            [
                                [
                                    "text" => "Конечно, должно происходить что-то новое",
                                    'callback_data' => 'q11_1',
                                ],
                            ],
                            [
                                [
                                    "text" => "Время от времени и только после обсуждения",
                                    'callback_data' => 'q11_2',
                                ],
                            ],
                            [
                                [
                                    "text" => "Нет, постель — не лаборатория",
                                    'callback_data' => 'q11_3',
                                ],
                            ],
                        ],
                    ], true)
                );
                $this->storeSet($this->chatId, 'question', $this->storeGet($this->chatId, 'question') . ' ' . $messageText);

                break;
            case 'q11':
                $this->component->sendPhoto(
                    $this->chatId,
                    'https://images.pexels.com/photos/6777798/pexels-photo-6777798.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
                    $this->render('question_12'),
                    json_encode([
                        'inline_keyboard' => [
                            [
                                [
                                    "text" => "Сначала должно быть «нормальное» примирение",
                                    'callback_data' => 'q12_1',
                                ],
                            ],
                            [
                                [
                                    "text" => "После скандала у меня нет желания заниматься сексом",
                                    'callback_data' => 'q12_2',
                                ],
                            ],
                            [
                                [
                                    "text" => "Да, потому что все становится ясно",
                                    'callback_data' => 'q12_3',
                                ],

                            ],
                        ],
                    ], true)
                );
                $this->storeSet($this->chatId, 'question', $this->storeGet($this->chatId, 'question') . ' ' . $messageText);

                break;
            case 'q12':
                $referralId = $this->storeGet($this->chatId, 'referralId');
                if ($referralId === null || (int)$referralId === $this->chatId) {
                    $this->send('refLink', ['chatId' => $this->chatId]);
                } else {
                    $thisChatQuestion = explode(' ', $this->storeGet($this->chatId, 'question'));
                    $refChatQuestion = explode(' ', $this->storeGet((int)$referralId, 'question'));

                    $count = 0;

                    foreach ($thisChatQuestion as $key => $value) {
                        if ($value === $refChatQuestion[$key]) {
                            $count++;
                        }
                    }

                    $percent = $count / count($thisChatQuestion) * 100;

                    $this->storeSet($this->chatId, 'question', null);
                    $this->storeSet((int)$referralId, 'question', null);

                    $this->component->sendMessage($this->chatId, 'Совместимость с вашим партнером ' . $percent . '%');
                    $this->component->sendMessage((int)$referralId, 'Совместимость с вашим партнером ' . $percent . '%');
                }

                break;
        }
    }
}
