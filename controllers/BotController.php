<?php


namespace app\controllers;


use app\components\TelegramComponent;
use app\models\bot\Bot;
use app\models\bot\BotSearch;
use app\models\bot\RegisterBotForm;
use app\models\User;
use RuntimeException;
use Throwable;
use yii\base\InvalidConfigException;
use yii\db\StaleObjectException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\httpclient\Exception;
use yii\web\Controller;
use yii\web\Response;

class BotController extends Controller
{

    /**
     * @return array[]
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => [],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [User::ROLE_USER],
                    ],
                    [
                        'actions' => ['create', 'delete', 'update', 'webhook'],
                        'allow' => true,
                        'roles' => [User::ROLE_MODER],
                    ],
                    [
                        'actions' => ['create', 'delete', 'update', 'webhook'],
                        'allow' => false,
                        'roles' => [User::ROLE_USER],
                        'denyCallback' => function ($rule, $action) {
                            return $action->controller->redirect(['bot/index']);
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'create' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new BotSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->get());

        return $this->render('index', compact('dataProvider'));
    }

    /**
     * @return string
     */
    public function actionCreate(): string
    {
        $model = new RegisterBotForm();
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            $botModel = new Bot([
                'token' => $model->token,
                'path' => $model->path,
                'title' => $model->title,
                'webhooks_is_set' => $model->webhooks_is_set,
                'controller_name' => $model->controller_name,
            ]);

            $botModel->save();

            $this->redirect(['index']);
        }

        return $this->render('_create', compact('model'));
    }

    /**
     * @param int $id
     * @return Response
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete(int $id): Response
    {
        $user = Bot::findOne($id);
        $user->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @param bool $webhook
     * @return Response
     * @throws Exception|InvalidConfigException
     */
    public function actionWebhook(int $id, bool $webhook): Response
    {
        $bot = Bot::findOne($id);
        $telegram = new TelegramComponent(['token' => $bot->token]);

        if ($webhook) {
            try {
                $telegram->deleteWebhook();
                Bot::updateAll(['webhooks_is_set' => Bot::WEBHOOKS_IS_NO_SET], ['id' => $id]);
            } catch (RuntimeException $exception) {
                throw new Exception('Не удалось удалить вебхуки');
            }
        } else {
            try {
                $telegram->setWebhook(Url::to([$bot->controller_name . '/index', 'token' => $telegram->token], true));
                Bot::updateAll(['webhooks_is_set' => Bot::WEBHOOKS_IS_SET], ['id' => $id]);
            } catch (RuntimeException $exception) {
                throw new Exception('Не удалось установить вебхуки');
            }
        }

        return $this->redirect(['index']);
    }

    public function actionUpdate(int $id)
    {
    }

}