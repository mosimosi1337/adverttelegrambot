<?php

namespace app\controllers;

use app\components\TelegramComponent;
use app\models\bot\Bot;
use app\models\User;
use RuntimeException;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\httpclient\Exception;
use yii\redis\Connection;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;

/**
 *
 * @property-read string $redisKeyTemplate
 * @property-read array $commands
 * @property-read Connection $redis
 */
abstract class AbstractBotController extends Controller
{
    /* @var int */
    protected const REDIS_KEY_LIFETIME_DEFAULT = 86400;

    /* @var string */
    protected const STORE_START = 'start';
    protected const STORE_MESSAGE_ID = 'message_id';
    protected const STORE_UPDATE_ID = 'offset';
    protected const GLOBAL_CHAT_ID = 0;

    /* @var bool */
    public $layout = false;
    /* @var TelegramComponent */
    protected $component;
    /* @var int */
    protected $chatId;

    /**
     * @return array
     */
    public function behaviors(): array
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => [],
            'rules' => [
                [
                    'actions' => ['index'],
                    'allow' => true,
                    'roles' => [],
                ],
                [
                    'actions' => ['set-webhooks', 'delete-webhooks'],
                    'allow' => true,
                    'roles' => [User::ROLE_ADMIN],
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * Init method
     */
    public function init()
    {
        $bot = Bot::find()->where(['controller_name' => $this->id])->one();
        $this->component = new TelegramComponent(['token' => $bot->token]);
    }

    /**
     * @return Connection
     */
    private function getRedis(): Connection
    {
        return \Yii::$app->redis;
    }

    /**
     * @return string
     */
    private function getRedisKeyTemplate(): string
    {
        return $this->id . ':%d:%s';
    }

    /**
     * @param int $chatId
     * @param string $name
     * @param string|null $value
     * @param int $lifetime
     */
    protected function storeSet(
        int     $chatId,
        string  $name,
        ?string $value,
        int     $lifetime = self::REDIS_KEY_LIFETIME_DEFAULT
    ): void
    {
        $redis = $this->getRedis();
        $key = sprintf($this->getRedisKeyTemplate(), $chatId, $name);

        if ($value === null && $redis->exists($key)) {
            $redis->del($key);
        } elseif ($value !== null) {
            $redis->set($key, $value);
            $redis->expire($key, $lifetime);
        }
    }

    /**
     * @param int $chatId
     * @param string $name
     * @return string|null
     */
    protected function storeGet(int $chatId, string $name): ?string
    {
        $redis = $this->getRedis();
        $key = sprintf($this->getRedisKeyTemplate(), $chatId, $name);

        return $redis->get($key);
    }


    /**
     * Метод отправкии сообщений
     * @param string $view
     * @param array $context
     * @return array
     * @throws Exception
     */
    protected function send(string $view, array $context = []): array
    {
        $message = $this->render(sprintf('//%s/%s', $this->id, $view), $context);

        return $this->component->sendMessage($this->chatId, $message);
    }

    /**
     * Метод удаления сообщений
     * @param int $chatId
     * @param int $messageId
     * @throws Exception
     */
    protected function delete(int $chatId, int $messageId): void
    {
        $this->component->deleteMessage($chatId, $messageId);
    }

    /**
     * Метод редактирования сообщения
     * @param string $view
     * @param array $context
     */
    protected function edit(string $view, array $context = []): void
    {
        $messageId = $this->storeGet($this->chatId, self::STORE_MESSAGE_ID);
        $message = $this->render(sprintf('//%s/%s', $this->id, $view), $context);
        $this->component->editMessageText($this->chatId, $messageId, $message);
    }

    /**
     * @throws ForbiddenHttpException
     * @throws InvalidConfigException
     */
    public function actionIndex(string $token)
    {
        if ($token !== $this->component->token) {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $params = \Yii::$app->request->getBodyParams();
        $updateId = $params['update_id'];
        $lastUpdateId = $this->storeGet(self::GLOBAL_CHAT_ID, self::STORE_UPDATE_ID);

        if ((int)$lastUpdateId === (int)$updateId) {
            return;
        }

        $this->storeSet(self::GLOBAL_CHAT_ID, self::STORE_UPDATE_ID, $updateId);

        if (isset($params['message'])) {
            $this->chatId = $params['message']['chat']['id'];
            $messageText = $params['message']['text'];
            $messageId = $params['message']['message_id'];
        } elseif (isset($params['callback_query'])) {
            $this->chatId = $params['callback_query']['message']['chat']['id'];
            $messageText = $params['callback_query']['data'];
            $messageId = $params['callback_query']['message']['message_id'];
        } else {
            return;
        }

        $arrayCommand = explode(' ', $messageText, 2);

        if ($arrayCommand[0] === '/start' && isset($arrayCommand[1])) {
            $this->storeSet($this->chatId, 'referralId', $arrayCommand[1]);
            $messageText = $arrayCommand[0];
        } elseif ($arrayCommand[0] === '/start' && !isset($arrayCommand[1])) {
            $this->storeSet($this->chatId, 'referralId', null);
        }

        if (array_key_exists($messageText, $this->commands)) {
            call_user_func([$this, $this->commands[$messageText]]);
        } else {
            $this->processMessage($messageText, $messageId);
        }

        return '';
    }

    /**
     * @param $messageText
     * @param $messageId
     * @return mixed
     */
    abstract public function processMessage($messageText, $messageId);

    /**
     * @return void
     */
    abstract public function commandStart(): void;

    /**
     * @return array
     */
    abstract function getCommands(): array;
}