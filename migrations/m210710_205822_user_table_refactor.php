<?php

use app\models\bot\Bot;
use yii\db\Migration;

/**
 * Class m210710_205822_user_table_refactor
 */
class m210710_205822_user_table_refactor extends Migration
{
    /**
     * @return bool|void
     */
    public function safeUp()
    {
        $this->createTable(Bot::tableName(), [
            'id' => $this->primaryKey(11),
            'path' => $this->string(),
            'token' => $this->string(),
            'title' => $this->string(),
            'webhooks_is_set' => $this->boolean(),
            'controller_name' => $this->string(),
        ]);
    }

    /**
     * @return bool
     */
    public function safeDown(): bool
    {
       $this->dropTable(Bot::tableName());

       return true;
    }
}
