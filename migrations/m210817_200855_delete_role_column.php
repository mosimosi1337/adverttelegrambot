<?php

use app\models\User;
use yii\db\Migration;

/**
 * Class m210817_200855_delete_role_column
 */
class m210817_200855_delete_role_column extends Migration
{
    /**
     * @return bool|void
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $user = \Yii::$app->authManager->createRole(User::ROLE_USER);
        \Yii::$app->authManager->add($user);

        $moder = \Yii::$app->authManager->createRole(User::ROLE_MODER);
        \Yii::$app->authManager->add($moder);
        \Yii::$app->authManager->addChild($moder, $user);

        $admin = \Yii::$app->authManager->createRole(User::ROLE_ADMIN);
        \Yii::$app->authManager->add($admin);
        \Yii::$app->authManager->addChild($admin, $moder);
    }

    /**
     * @return false
     */
    public function safeDown(): bool
    {
        echo "m210817_200855_delete_role_column cannot be reverted.\n";

        return false;
    }
}
