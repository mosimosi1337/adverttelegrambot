<?php


namespace app\components;

use \yii\base\Component;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;
use yii\httpclient\Exception;
use \yii\httpclient\Response;


/**
 *
 * @property-write string $webhook
 * @property-read null|array $updates
 */
class TelegramComponent extends Component
{
    /** @var string */
    public string $token;
    /** @var Client */
    private Client $client;

    /** URL для создания запроса боту */
    private const BASE_URL = 'https://api.telegram.org/bot%s';

    /** @var string */
    private const METHOD_GET_UPDATES = 'getUpdates';
    private const METHOD_SEND_MESSAGE = 'sendMessage';
    private const METHOD_SET_WEBHOOK = 'setWebhook';
    private const METHOD_DELETE_WEBHOOK = 'deleteWebhook';
    private const METHOD_DELETE_MESSAGE = 'deleteMessage';
    private const METHOD_EDIT_MESSAGE = 'editMessageText';
    private const METHOD_EDIT_MEDIA = 'editMessageMedia';
    private const METHOD_SEND_PHOTO = 'sendPhoto';

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        if ($this->token === null) {
            throw new InvalidConfigException('Telegram bot token is undefined.');
        }
        $this->client = new Client([
            'baseUrl' => sprintf(self::BASE_URL, $this->token),
        ]);
    }

    /**
     * Метод телеграмма для отправки сообщения пользователю
     * @param int $chatId
     * @param string $message
     * @return array
     * @throws Exception
     */
    public function sendMessage(int $chatId, string $message): array
    {
        $params = [
            'chat_id' => $chatId,
            'text' => $message,
            'parse_mode' => 'html',
        ];
        $response = $this->client
            ->get(self::METHOD_SEND_MESSAGE, $params)
            ->send();

        $this->processResponse($response);
        return $response->getData();
    }

    /**
     * @param int $chatId
     * @param string|null $caption
     * @param string $url
     * @return array
     * @throws Exception
     */
    public function sendPhoto(int $chatId, string $url, ?string $caption, ?string $reply_markup): array
    {
        $params = [
            'chat_id' => $chatId,
            'photo' => $url,
            'caption' => $caption,
            'parse_mode' => 'html',
            'reply_markup' => $reply_markup,
        ];
        $response = $this->client
            ->get(self::METHOD_SEND_PHOTO, $params)
            ->send();

        $this->processResponse($response);
        return $response->getData();
    }

    /**
     * Метод изменения собщения
     * @param int $chatId
     * @param int $messageId
     * @param string $text
     * @return void
     */
    public function editMessageText(int $chatId, int $messageId, string $text, array $replyMarkup = null): void
    {
        $params = [
            'chat_id' => $chatId,
            'message_id' => $messageId,
            'text' => $text,
            'parse_mode' => 'html',
            'reply_markup' => $replyMarkup,
        ];

        $response = $this->client
            ->get(self::METHOD_EDIT_MESSAGE, $params)
            ->send();

        $this->processResponse($response);
    }

    public function editMessageMedia(int $chatId, int $messageId, string $inputMedia, array $replyMarkup = null): void
    {
        $response = $this->client
            ->get(self::METHOD_EDIT_MEDIA, [
                'chat_id' => $chatId,
                'message_id' => $messageId,
                'media' => $inputMedia,
                'reply_markup' => $replyMarkup,
            ])
            ->send();

        $this->processResponse($response);
    }

    /**
     * Метод удаления сообщения из чата
     * @param int $chatId
     * @param int $messageId
     * @return void
     * @throws Exception
     */
    public function deleteMessage(int $chatId, int $messageId): void
    {
        $response = $this->client
            ->get(self::METHOD_DELETE_MESSAGE, ['chat_id' => $chatId, 'message_id' => $messageId])
            ->send();

        $this->processResponse($response);
    }

    /**
     * Получение обновлений используя телеграмм метод getUpdates()
     * @return null | array
     */
    public function getUpdates(): ?array
    {
        $response = $this->client
            ->get(self::METHOD_GET_UPDATES)
            ->send();
        return $response->getData();
    }

    /**
     * Метод установки вубхуков
     * @param string $url
     * @return void
     * @throws \yii\base\InvalidConfigException
     * @throws Exception
     */
    public function setWebhook(string $url): void
    {
        $response = $this->client
            ->post(self::METHOD_SET_WEBHOOK, ['url' => $url])
            ->send();
        $this->processResponse($response);
    }

    /**
     * Метод удаления вебхуков
     * @return void
     */
    public function deleteWebhook(): void
    {
        $response = $this->client
            ->post(self::METHOD_DELETE_WEBHOOK)
            ->send();
        $this->processResponse($response);
    }

    /**
     * @param Response $response
     */
    private function processResponse(Response $response): void
    {
        $data = $response->getData();

        if (!$data['ok']) {
            throw new \RuntimeException($data['description'], $data['error_code']);
        }
    }
}