<?php

namespace app\models\user;

use app\models\User;

class RegisterUserForm extends User
{
    /** @var ?string */
    public $role;


    /**
     * @return array[]
     */
    public function rules(): array
    {
        return [
            [
                [
                    'username',
                    'email',
                    'password',
                    'role',
                ],
                'string',
            ],
            [
                [
                    'username',
                    'email',
                    'password',
                ],
                'required',
            ],
            [
                [
                    'email',
                ],
                'email',
            ],
            [
                [
                    'role',
                ],
                'in',
                'range' => array_keys(User::roles()),
            ],
        ];
    }

}