<?php

namespace app\models\bot;

class RegisterBotForm extends Bot
{
    /** @var ?string */
    public $token;
    /** @var ?string */
    public $path;
    /** @var ?string */
    public $title;
    /** @var string */
    public $controller_name;
    /** @var ?bool */
    public $webhooks_is_set;

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [
                [
                    'title',
                    'path',
                    'token',
                    'webhooks_is_set',
                    'controller_name'
                ],
                'required',
            ],
            [
                [
                    'webhooks_is_set',
                ],
                'boolean',
            ],
            [
                [
                    'title',
                    'path',
                    'token',
                    'controller_name'
                ],
                'string',
            ],
        ];
    }
}