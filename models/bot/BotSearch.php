<?php

namespace app\models\bot;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class BotSearch extends Model
{

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query = Bot::find();

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}