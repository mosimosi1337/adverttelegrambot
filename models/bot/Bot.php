<?php

namespace app\models\bot;

use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $controller_name
 * @property string $token
 * @property string $title
 * @property string $path
 * @property bool webhooks_is_set
 */
class Bot extends ActiveRecord
{
    /** @var int */
    const WEBHOOKS_IS_SET = 1;
    /** @var int */
    const WEBHOOKS_IS_NO_SET = 0;

    /**
     * @param int $webhooks_is_set
     * @return string
     */
    public static function getWebhooksHtml(int $webhooks_is_set): string
    {
        switch ($webhooks_is_set){
            case self::WEBHOOKS_IS_SET:
                return 'Удалить';
            default:
            case self::WEBHOOKS_IS_NO_SET:
                return 'Установить';
        }
    }

    /**
     * @return string[]
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'token' => 'Токен',
            'title' => 'Название',
            'path' => 'Ссылка',
            'webhooks_is_set' => 'Вебхуки',
            'controller_name' => 'Название контроллера',
        ];
    }
}